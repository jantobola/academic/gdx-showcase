attribute vec3 a_position;
attribute vec2 a_texCoord;

varying vec2 out_tex;

uniform mat4 worldMatrix;
uniform mat4 viewMatrix;
uniform mat4 projectionMatrix;

void main()
{
    gl_Position = projectionMatrix * viewMatrix * worldMatrix * vec4(a_position, 1.0);
    out_tex = a_texCoord;
}