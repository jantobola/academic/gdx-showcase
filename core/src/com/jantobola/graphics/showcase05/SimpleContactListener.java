package com.jantobola.graphics.showcase05;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.physics.bullet.collision.ContactListener;

/**
 * SimpleContactListener
 *
 * @author Jan Tobola, 2015
 */
public class SimpleContactListener extends ContactListener {

	Music woodHit = Gdx.audio.newMusic(Gdx.files.internal("sounds/wood_hit.mp3"));

	@Override
	public boolean onContactAdded(int userValue0, int partId0, int index0, int userValue1, int partId1, int index1) {
		if(!woodHit.isPlaying()) {
			woodHit.play();
		}

		return true;
	}

	public void dispose() {
		woodHit.dispose();
	}

}
