package com.jantobola.graphics.showcase02;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.*;
import com.badlogic.gdx.graphics.g3d.utils.FirstPersonCameraController;
import com.badlogic.gdx.graphics.glutils.ShaderProgram;
import com.badlogic.gdx.utils.GdxRuntimeException;

/**
 * Custom shaders
 *
 * Remarks:
 *
 */
public class Showcase02 extends ApplicationAdapter {

	private Mesh cube;
	private ShaderProgram shader;

	private PerspectiveCamera cam;
	private FirstPersonCameraController cntrl;

	@Override
	public void create() {

		// @formatter:off

//			  CUBE GEOMETRY
//			"""""""""""""""""
//				y
//				|[4]         [5]
//				o * * * * * o
//			   *|          **
//			  * |     [7] * *
//		 [6] o *|*_*_*_*_o__*__ x
//			 * /o [0]    *  o [1]
//			 */          * *
//			 *	         **
//		    /o * * * * * o
//		   /[2]			  [3]
//		  z

		float[] vertices = new float[]{
				-0.5f, -0.5f, -0.5f, Color.RED.toFloatBits(), 		// [0]
				+0.5f, -0.5f, -0.5f, Color.GREEN.toFloatBits(),		// [1]
				-0.5f, -0.5f, +0.5f, Color.BLUE.toFloatBits(),		// [2]
				+0.5f, -0.5f, +0.5f, Color.WHITE.toFloatBits(),		// [3]

				-0.5f, +0.5f, -0.5f, Color.RED.toFloatBits(),		// [4]
				+0.5f, +0.5f, -0.5f, Color.GREEN.toFloatBits(),		// [5]
				-0.5f, +0.5f, +0.5f, Color.BLUE.toFloatBits(),		// [6]
				+0.5f, +0.5f, +0.5f, Color.WHITE.toFloatBits()		// [7]
		};

		short[] indices = new short[]{
				// [front]
				2, 3, 6,
				6, 3, 7,
				// [top]
				6, 7, 4,
				4, 7, 5,
				// [back]
				1, 0, 5,
				5, 0, 4,
				// [bottom]
				2, 0, 3,
				3, 0, 1,
				// [left]
				0, 2, 4,
				4, 2, 6,
				// [right]
				3, 1, 7,
				7, 1, 5
		};

		cube = new Mesh(true, vertices.length, indices.length,
				new VertexAttributes(
						new VertexAttribute(VertexAttributes.Usage.Position, 3, ShaderProgram.POSITION_ATTRIBUTE),
						new VertexAttribute(VertexAttributes.Usage.ColorPacked, 4, ShaderProgram.COLOR_ATTRIBUTE)
				)
		);

		cube.setVertices(vertices);
		cube.setIndices(indices);

		// @formatter:on

		// ------------ camera -------------

		cam = new PerspectiveCamera(65, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
		cam.position.z = 3;
		cam.lookAt(0, 0, 0);

		cam.near = 0.1f;
		cam.far = 100;

		cntrl = new FirstPersonCameraController(cam);
		Gdx.input.setInputProcessor(cntrl);

		// ------------ shaders -------------

		shader = new ShaderProgram(Gdx.files.internal("shaders/showcase02/VS_Basic.glsl"), Gdx.files.internal("shaders/showcase02/PS_Basic.glsl"));
		if (!shader.isCompiled())
			throw new GdxRuntimeException("Couldn't compile shader: " + shader.getLog());

		// ------------ inition rendering context -----------

		Gdx.gl.glViewport(0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
		Gdx.gl.glEnable(GL20.GL_DEPTH_TEST);
		Gdx.gl.glEnable(GL20.GL_CULL_FACE);
		Gdx.gl.glCullFace(GL20.GL_BACK);
		Gdx.gl.glDepthFunc(GL20.GL_LESS);
		Gdx.gl.glDepthRangef(0f, 1f);

	}

	@Override
	public void render() {

		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT);
		cntrl.update();

		// @formatter:off

		shader.begin();
			shader.setUniformMatrix("mvp", cam.combined);
			cube.render(shader, GL20.GL_TRIANGLES);
		shader.end();

		// @formatter:on
	}

	@Override
	public void dispose() {
		cube.dispose();
		shader.dispose();
	}
}
